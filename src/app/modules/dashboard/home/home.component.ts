import { Component, OnInit } from '@angular/core';

// import { ApiService } from '../../../common/services/api.service';
// import { CONSTANTS } from '../../../constants/app-constants.constant';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // constructor(private _apiService: ApiService) { }

  ngOnInit() {
    // NOTE: HOW TO MAKE API CALLS EXAMPLE
    // this._apiService.get(CONSTANTS.API.userPage)
    // .subscribe( data => {
    //   console.log(data);
    // }, error => {
    //   console.log('Error occured', error)
    // });
  }

}
