import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { SharedService } from '../../../common/services/shared.service';
import { CONSTANTS } from '../../../constants/app-constants.constant';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit, OnDestroy {

  APP_CONSTANTS = CONSTANTS;
  signupPage: boolean;
  routerSubscription: Subscription
  constructor(
    public _sharedService: SharedService,
    private router: Router
    ) { }
  
  ngOnInit() {
    this.routerSubscription =  this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((event: NavigationEnd) => {
      this.signupPage = (event.urlAfterRedirects === CONSTANTS.APP_ROOTS.signupPage);
    });
  }

  ngOnDestroy() {
    this.routerSubscription.unsubscribe();
  }

  logout() {
    localStorage.removeItem(CONSTANTS.LOCAL_STORAGE.authToken)
    this.router.navigate([CONSTANTS.APP_ROOTS.loginPage])
  }
}
