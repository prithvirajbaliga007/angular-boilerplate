import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { ToastMesageService } from './toast-mesage.service';
import { ToastMessage } from '../../../common/models/common.model';

@Component({
  selector: 'app-toast-message',
  templateUrl: './toast-message.component.html',
  styleUrls: ['./toast-message.component.scss']
})
export class ToastMessageComponent implements OnInit, OnDestroy {

  toast: ToastMessage;
  private subscription: Subscription;
  
  constructor(private _toastMesageService: ToastMesageService) { }

  // NOTE: How to use
  // this._ToastMesageService.setToastMessage({
  //   message: 'Toast Message',
  //   type: 'success or error'
  // })

  ngOnInit() {
    this.setToastValue();
    this.subscription = this._toastMesageService.showToastState
    .subscribe((toast: ToastMessage) => {
      if(toast.message !== '') {
        this.setToastValue(toast.message, toast.type);
        setTimeout(() => this.setToastValue(), 6000);
      }
    })
  }

  setToastValue = (message: string = '', type: string = '') => {
    this.toast = { message, type }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
