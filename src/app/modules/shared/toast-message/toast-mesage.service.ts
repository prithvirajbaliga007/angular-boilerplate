import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { ToastMessage } from '../../../common/models/common.model';

@Injectable({
  providedIn: 'root'
})
export class ToastMesageService {

  constructor() { }

  private showToastMessageSubject = new BehaviorSubject<ToastMessage>({
    message: '',
    type: ''
  });
  showToastState = this.showToastMessageSubject.asObservable();

  setToastMessage = (toastMessage: ToastMessage) => {
    this.showToastMessageSubject.next(toastMessage);
  }
}
