import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoaderState } from '../../../common/models/common.model';
import { LoaderService } from './loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit, OnDestroy {

  show = false;
  private subscription: Subscription;

  constructor(private _loaderService: LoaderService) { }

  // NOTE: How to use
  // to show the loader -> this._loaderService.show();
  // to hide the loader -> this._loaderService.hide();

  ngOnInit() {
    this.subscription = this._loaderService.loaderState
    .subscribe((state: LoaderState) => {
      this.show = state.show;
    });
  }
  
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
