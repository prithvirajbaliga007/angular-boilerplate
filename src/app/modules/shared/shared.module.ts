import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { LoaderComponent } from './loader/loader.component';
import { ToastMessageComponent } from './toast-message/toast-message.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';

@NgModule({
  declarations: [
    LoaderComponent,
    ToastMessageComponent,
    NavBarComponent
  ],
  imports: [
    CommonModule,
    RouterModule 
  ],
  exports: [
    LoaderComponent,
    ToastMessageComponent,
    NavBarComponent
  ]
})
export class SharedModule { }
