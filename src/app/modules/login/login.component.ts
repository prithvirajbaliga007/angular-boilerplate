import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { CONSTANTS } from '../../constants/app-constants.constant';
import { SharedService } from 'src/app/common/services/shared.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  LOGIN_CONSTANTS = CONSTANTS
  loginForm: FormGroup;
  loginFormInvalidStatus: boolean;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    public _sharedService: SharedService
  ) { }

  ngOnInit() {
    this.loginFormInvalidStatus = false;
    this.createLoginFormInstance();
  }

  createLoginFormInstance = () => {
    this.loginForm = this.fb.group({
      email: ['', [ Validators.required, Validators.pattern(this.LOGIN_CONSTANTS.REGX.email) ]],
      password: ['', [ Validators.required, Validators.pattern(this.LOGIN_CONSTANTS.REGX.password) ]]
    });
  }

  onSubmit = () => {
    if (this.loginForm.valid) {
      this.loginFormInvalidStatus = false;
      localStorage.setItem(this.LOGIN_CONSTANTS.LOCAL_STORAGE.authToken, this._sharedService.fc(this.loginForm).email.value);
      this.router.navigate([this.LOGIN_CONSTANTS.APP_ROOTS.homePage]);
    } else {
      this.loginFormInvalidStatus = true;
    }
  }

}
