import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login.component';
import { LoginRedirectGuard } from '../../guards/login-redirect.guard';

const routes: Routes = [
  { 
    path: '', 
    component: LoginComponent,
    canActivate: [LoginRedirectGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
