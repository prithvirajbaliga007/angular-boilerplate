import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { ForgotPassowrdComponent } from './forgot-passowrd/forgot-passowrd.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SignupComponent } from './signup/signup.component';

const routes: Routes = [
  { path: 'email-verification/:id', component: EmailVerificationComponent },
  { path: 'forgot-password', component: ForgotPassowrdComponent },
  { path: 'change-password/:id', component: ChangePasswordComponent },
  { path: 'sign-up', component: SignupComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
