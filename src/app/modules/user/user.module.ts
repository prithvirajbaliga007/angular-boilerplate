import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { ForgotPassowrdComponent } from './forgot-passowrd/forgot-passowrd.component';
import { SignupComponent } from './signup/signup.component';

@NgModule({
  declarations: [
    EmailVerificationComponent,
    ForgotPassowrdComponent,
    ChangePasswordComponent,
    SignupComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule
  ]
})
export class UserModule { }
