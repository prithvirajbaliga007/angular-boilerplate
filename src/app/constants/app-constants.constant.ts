
export const CONSTANTS = {
    APP_ROOTS: {
        homePage: '/home',
        loginPage: '/login',
        signupPage: '/user/sign-up',
    },
    API : {
        userPage: 'users'
    },
    LOGIN_FORM: {
        emailErrorMessage: 'Please enter valid email',
        passwordErrorMessage: 'Please enter valid password',
        submitButton: 'LOGIN'
    },
    REGX: {
        email: /^(\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        password: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/
    },
    LOCAL_STORAGE : {
        authToken: 'authToken'
    }
};