import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private httpClient: HttpClient) { }

  setParams = (paramsObj: object): HttpParams => {
    let params = new HttpParams();
    for (let key in paramsObj) {
      params = params.append(key, paramsObj[key])
    }
    return params;
  }

  get = (url: string, paramsObj: object = null): Observable<any> => {
    const params = paramsObj ? this.setParams(paramsObj) : null;
    return this.httpClient.get<any>(url, { params })
  }

  delete = (url: string, paramsObj: object = null): Observable<any> => {
    const params = paramsObj ? this.setParams(paramsObj) : null;
    return this.httpClient.delete<any>(url, { params });
  }

  post = (url: string, body: any): Observable<any> => {
    return this.httpClient.post<any>(url, body)
  }

  put = (url: string, body: any): Observable<any> => {
    return this.httpClient.put<any>(url, body)
  }

}
