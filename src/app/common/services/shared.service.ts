import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';

import { CONSTANTS } from '../../constants/app-constants.constant';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor() { }

  fc(form) { return form.controls; }

  formFieldErrorMessage(control: AbstractControl, formStatus: boolean): Boolean {
    return (formStatus && control.invalid) || (control.invalid && control.touched);
  }

  get getAuthToken() {
    return localStorage.getItem(CONSTANTS.LOCAL_STORAGE.authToken);
  }

}
