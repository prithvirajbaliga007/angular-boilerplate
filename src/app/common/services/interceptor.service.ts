import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, timeout, finalize } from 'rxjs/operators';

import { SharedService } from './shared.service';
import { ENV_APP_CONSTANT } from '../../../environments/environment';
import { LoaderService } from '../../modules/shared/loader/loader.service';
import { ToastMesageService } from '../../modules/shared/toast-message/toast-mesage.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(
    private _sharedService: SharedService, 
    private _loaderService: LoaderService
    ) { }
  
  handleError = (error: HttpErrorResponse) => {
    return throwError(error);
  };

  intercept(request: HttpRequest<any>, next: HttpHandler): 
  Observable<HttpEvent<any>> {

    this._loaderService.show();
    const token = this._sharedService.getAuthToken;
    let headers =  new HttpHeaders({ 'Content-Type': 'application/json' });

    if(token) {
      headers = headers.append('Authorization', token)
    }
    
    return next.handle( request.clone({ 
      url:  ENV_APP_CONSTANT.baseApiUrl + request.url,
      headers: headers
    }) )
    .pipe(
      timeout(ENV_APP_CONSTANT.apiTimeOut),
      retry(ENV_APP_CONSTANT.apiRetryCount),
      catchError(this.handleError),
      finalize(() => this._loaderService.hide())
    );
  }
}
