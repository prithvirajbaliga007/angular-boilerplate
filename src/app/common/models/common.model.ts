export interface LoaderState {
    show: boolean;
}

export interface ToastMessage { 
    message: string, 
    type: string 
}