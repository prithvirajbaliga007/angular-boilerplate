export const environment = {
  production: true
};

// NOTE: Used in production only
export const ENV_APP_CONSTANT = {
  apiVersionUrl: 'v1/',
  baseApiUrl: 'https://jsonplaceholder.typicode.com/',
  apiTimeOut: 10000, // 10 seconds * 3 retry's = 40 seconds
  apiRetryCount: 3
};